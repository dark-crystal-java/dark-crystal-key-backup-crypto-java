package org.magmacollective.darkcrystal.keybackup.crypto;

import org.whispersystems.curve25519.Curve25519KeyPair;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class KeyBackupCryptoTest {
  @Test
  @DisplayName("Test DH agreements")
  public void testAgreements() {
    final Curve25519KeyPair senderKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();

    final byte[] sharedSecretReciever = KeyBackupCrypto.calculateAgreement(senderKeyPair.getPublicKey(), recieverKeyPair.getPrivateKey());
    final byte[] sharedSecretSender = KeyBackupCrypto.calculateAgreement(recieverKeyPair.getPublicKey(), senderKeyPair.getPrivateKey());
    assertTrue(Arrays.equals(sharedSecretReciever, sharedSecretSender));
  }

  @Test
  @DisplayName("Test DH encryption")
  public void testDhEncryption() throws GeneralSecurityException {
    final Curve25519KeyPair senderKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final byte[] message = "its nice to be important but its more important to be nice".getBytes();

    final byte[] ciphertext = KeyBackupCrypto.box(message, senderKeyPair, recieverKeyPair.getPublicKey());
    assertFalse(Arrays.equals(message, ciphertext));
    final byte[] result = KeyBackupCrypto.unbox(ciphertext, recieverKeyPair, senderKeyPair.getPublicKey());
    assertTrue(Arrays.equals(message, result), "Correct message decrypted");
  }

  @Test
  @DisplayName("Test DH encryption with additional context")
  public void testDhEncryptionWithContext() throws GeneralSecurityException {
    final Curve25519KeyPair senderKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final byte[] message = "its nice to be important but its more important to be nice".getBytes();
    final byte[] context = "this is additional information known by both parties".getBytes();

    final byte[] ciphertext = KeyBackupCrypto.box(message, senderKeyPair, recieverKeyPair.getPublicKey(), context);
    assertFalse(Arrays.equals(message, ciphertext));
    final byte[] result = KeyBackupCrypto.unbox(ciphertext, recieverKeyPair, senderKeyPair.getPublicKey(), context);
    assertTrue(Arrays.equals(message, result));
  }

  @Test
  @DisplayName("Test DH encryption - fails on wrong ciphertext")
  public void testDhEncryptionFails() {
    final Curve25519KeyPair senderKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final byte[] message = "its nice to be important but its more important to be nice".getBytes();

    final byte[] ciphertext = KeyBackupCrypto.box(message, senderKeyPair, recieverKeyPair.getPublicKey());
    assertFalse(Arrays.equals(message, ciphertext));
    // modify ciphertext:
    ciphertext[0] = 1;
    ciphertext[1] = 0;
    assertThrows(GeneralSecurityException.class, () -> {
      KeyBackupCrypto.unbox(ciphertext, recieverKeyPair, senderKeyPair.getPublicKey());
    });
  }

  @Test
  @DisplayName("Test DH encryption with additional context - fails on wrong ciphertext")
  public void testDhEncryptionWithContextWrongCiphertext() {
    final Curve25519KeyPair senderKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final byte[] message = "its nice to be important but its more important to be nice".getBytes();
    final byte[] context = "this is additional information known by both parties".getBytes();

    final byte[] ciphertext = KeyBackupCrypto.box(message, senderKeyPair, recieverKeyPair.getPublicKey(), context);
    assertFalse(Arrays.equals(message, ciphertext));
    // modify ciphertext:
    ciphertext[0] = 1;
    ciphertext[1] = 0;
    assertThrows(GeneralSecurityException.class, () -> {
      KeyBackupCrypto.unbox(ciphertext, recieverKeyPair, senderKeyPair.getPublicKey(), context);
    });
  }

  @Test
  @DisplayName("Test one way box and unbox")
  public void testOneWayBoxAndUnbox() throws GeneralSecurityException {
    final Curve25519KeyPair recieverKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
    final byte[] message = "cabbage".getBytes();

    final byte[] ciphertext = KeyBackupCrypto.oneWayBox(message, recieverKeyPair.getPublicKey());
    assertFalse(Arrays.equals(message, ciphertext));
    final byte[] result = KeyBackupCrypto.oneWayUnbox(ciphertext, recieverKeyPair);
    assertTrue(Arrays.equals(message, result), "Decryption successful");
  }

  @Test
  @DisplayName("Test Blake2b hash")
  public void testBlake2bHash() {
    final byte[] message = "irgendwas".getBytes();
    final byte[] hash = KeyBackupCrypto.blake2b(message);
    final byte[] message2 = "irgendwas".getBytes();
    final byte[] hash2 = KeyBackupCrypto.blake2b(message2);
    assertTrue(Arrays.equals(hash, hash2));
  }

  @Test
  @DisplayName("Test Blake2b keyed hash")
  public void testBlake2bKeyedHash() {
    KeyBackupCrypto keyBackupCrypto = new KeyBackupCrypto();
    final byte[] message = "irgendwas".getBytes();
    final byte[] key = "this is sure a 32byte key really".getBytes();
    final byte[] hash = keyBackupCrypto.blake2b(message, key);

    final byte[] message2 = "irgendwas".getBytes();
    final byte[] key2 = "this is sure a 32byte key really".getBytes();
    final byte[] hash2 = keyBackupCrypto.blake2b(message2, key2);

    assertTrue(Arrays.equals(hash, hash2));
  }

  @Test
  @DisplayName("Test hex string encoding")
  public void testHexStringEncoding() {
    byte[] message = "irgendwas".getBytes();
    String messageHex = KeyBackupCrypto.toHexString(message);
    assertEquals(messageHex.length(), message.length * 2, "Encoded string has correct length");
    assertArrayEquals(KeyBackupCrypto.fromHexString(messageHex), message, "Decoding successful");
  }

  @Test
  @DisplayName("Test Base64 string encoding")
  public void testBase64StringEncoding() {
    byte[] message = "irgendwas".getBytes();
    String messageBase64 = KeyBackupCrypto.toBase64String(message);
    assertArrayEquals(KeyBackupCrypto.fromBase64String(messageBase64), message, "Decoding successful");
  }
}
