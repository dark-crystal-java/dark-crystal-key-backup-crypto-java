package org.magmacollective.darkcrystal.keybackup.crypto;

import net.i2p.crypto.eddsa.EdDSAPublicKey;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.security.KeyPair;
import java.security.PublicKey;

import static org.junit.jupiter.api.Assertions.*;

public class EdDSATest {
  @Test
  @DisplayName("Test signing and verification")
  public void testSigning() throws Exception {
    final byte[] message = "cabbage".getBytes();
    EdDSA edDSA = new EdDSA();

    KeyPair keyPair = edDSA.generateKeyPair();

    final byte[] signedMessage = edDSA.signMessage(message, keyPair.getPrivate());
    assertTrue(signedMessage.length == 64 + message.length);
    assertTrue(edDSA.verifyMessage(signedMessage, keyPair.getPublic()));
    assertArrayEquals(edDSA.detachMessage(signedMessage), message);
  }

  @Test
  @DisplayName("Test signing and verification with imported public key")
  public void testSigningWithImportedKey() throws Exception {
    final byte[] message = "cabbage".getBytes();
    EdDSA edDSA = new EdDSA();

    KeyPair keyPair = edDSA.generateKeyPair();

    final byte[] signedMessage = edDSA.signMessage(message, keyPair.getPrivate());
    assertTrue(signedMessage.length == 64 + message.length);
    byte[] publicKeyBytes = edDSA.PublicKeyToBytes(keyPair.getPublic());
    PublicKey importedPublicKey = edDSA.PublicKeyFromBytes(publicKeyBytes);
    assertTrue(edDSA.verifyMessage(signedMessage, importedPublicKey));
    assertArrayEquals(edDSA.detachMessage(signedMessage), message);
  }

  @Test
  @DisplayName("Test signing and verification - fails on wrong public key")
  public void testSigningWrongPublicKey() throws Exception {
    final byte[] message = "cabbage".getBytes();
    EdDSA edDSA = new EdDSA();

    KeyPair keyPair = edDSA.generateKeyPair();

    final byte[] signedMessage = edDSA.signMessage(message, keyPair.getPrivate());
    assertTrue(signedMessage.length == 64 + message.length);
    assertTrue(edDSA.verifyMessage(signedMessage, keyPair.getPublic()));
    assertFalse(edDSA.verifyMessage(signedMessage, edDSA.generateKeyPair().getPublic()));
  }

  @Test
  @DisplayName("Test signing and verification - detached signature")
  public void testSigningDetached() throws Exception {
    final byte[] message = "cabbage".getBytes();
    EdDSA edDSA = new EdDSA();

    KeyPair keyPair = edDSA.generateKeyPair();

    final byte[] signature = edDSA.signMessageDetached(message, keyPair.getPrivate());
    assertTrue(signature.length == 64, "Signature has correct length");
    assertTrue(edDSA.verifyMessageDetached(message, keyPair.getPublic(), signature));
    assertFalse(edDSA.verifyMessageDetached(message, edDSA.generateKeyPair().getPublic(), signature));
  }
}