package org.magmacollective.darkcrystal.keybackup.crypto;

import org.whispersystems.curve25519.Curve25519KeyPair;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

public class KeyBackupCryptoMain {
  public static void main(String args[]) {
    KeyBackupCrypto keyBackupCrypto = new KeyBackupCrypto();

    final Curve25519KeyPair senderKeyPair = keyBackupCrypto.generateCurve25519Keypair();

    System.out.println("Generating Sender Curve25519 Keypair");
    System.out.println("Public Key: " + bytesToHex(senderKeyPair.getPublicKey()));
    System.out.println("Secret Key: " + bytesToHex(senderKeyPair.getPrivateKey()));

    final Curve25519KeyPair recieverKeyPair = keyBackupCrypto.generateCurve25519Keypair();

    System.out.println("Generating Receiver Curve25519 Keypair");
    System.out.println("Public Key: " + bytesToHex(recieverKeyPair.getPublicKey()));
    System.out.println("Secret Key: " + bytesToHex(recieverKeyPair.getPrivateKey()));

    final byte[] sharedSecretReciever = keyBackupCrypto.calculateAgreement(senderKeyPair.getPublicKey(), recieverKeyPair.getPrivateKey());
    final byte[] sharedSecretSender = keyBackupCrypto.calculateAgreement(recieverKeyPair.getPublicKey(), senderKeyPair.getPrivateKey());
    System.out.println("Shared secret for sender: " + bytesToHex(sharedSecretSender));
    System.out.println("Shared secret for reciever: " + bytesToHex(sharedSecretReciever));
  }

  private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

  public static String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = HEX_ARRAY[v >>> 4];
      hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
    }
    return new String(hexChars);
  }

}
