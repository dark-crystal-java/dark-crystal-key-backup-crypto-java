package org.magmacollective.darkcrystal.keybackup.crypto;

import net.i2p.crypto.eddsa.EdDSAEngine;
import net.i2p.crypto.eddsa.EdDSAPublicKey;
import net.i2p.crypto.eddsa.EdDSASecurityProvider;
import net.i2p.crypto.eddsa.spec.EdDSAGenParameterSpec;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveSpec;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAPublicKeySpec;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;

import static org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto.byteArrayConcat;

/**
 * Ed25519 Key generation, signing and validation
 */

public class EdDSA {
  public static final int SIGNATURE_LENGTH = 64;
  public static final int PUBLIC_KEY_LENGTH = 32;
  public EdDSA () {
    Security.addProvider(new EdDSASecurityProvider());
  }
  private static final EdDSANamedCurveSpec CURVE_SPEC = EdDSANamedCurveTable.getByName("Ed25519");

  /**
   * Generate a keypair
   * @return A keypair
   * @throws Exception
   */
  public KeyPair generateKeyPair() throws Exception {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EdDSA");
    return keyGen.generateKeyPair();
  }

  public byte[] PublicKeyToBytes(PublicKey publicKey) {
    EdDSAPublicKey pk = (EdDSAPublicKey) publicKey;
    return pk.getAbyte();
  }

  public PublicKey PublicKeyFromBytes(byte[] publicKeyBytes) throws Exception {
    return new EdDSAPublicKey(
            new EdDSAPublicKeySpec(publicKeyBytes, CURVE_SPEC));
  }

  /**
   * Generate a signature for a given message, and return the signature attached to the message
   * @param message
   * @param privateKey
   * @return signed message
   * @throws Exception
   */
  public byte[] signMessage(byte[] message, PrivateKey privateKey) throws Exception {
    return byteArrayConcat(signMessageDetached(message, privateKey), message);
  }

  /**
   * Verify a given signed message
   * @param messageWithSignature
   * @param publicKey
   * @return whether the message is valid or not
   * @throws Exception
   */
  public Boolean verifyMessage(byte[] messageWithSignature, PublicKey publicKey) throws Exception {
    byte[] signature = new byte[SIGNATURE_LENGTH];
    byte[] message = new byte[messageWithSignature.length - SIGNATURE_LENGTH];
    System.arraycopy(messageWithSignature, 0, signature, 0, SIGNATURE_LENGTH);
    System.arraycopy(messageWithSignature, SIGNATURE_LENGTH, message, 0, message.length);
    return verifyMessageDetached(message, publicKey, signature);
  }

  /**
   * Remove a signature from a message
   * @param messageWithSignature
   * @return the message without a signature
   */
  public static byte[] detachMessage(byte[] messageWithSignature) {
    byte[] message = new byte[messageWithSignature.length - SIGNATURE_LENGTH];
    System.arraycopy(messageWithSignature, SIGNATURE_LENGTH, message, 0, message.length);
    return message;
  }

  /**
   * Generate a signature for the given message
   * @param message
   * @param privateKey
   * @return a signature
   * @throws Exception
   */
  public byte[] signMessageDetached(byte[] message, PrivateKey privateKey) throws Exception {
    Signature signer = new EdDSAEngine(MessageDigest.getInstance("SHA-512"));
    signer.initSign(privateKey);
    signer.update(message);
    return signer.sign();
  }

  /**
   * Verify A given message with a given signature
   * @param message
   * @param publicKey
   * @param signature
   * @return whether the signature is valid or not
   * @throws Exception
   */
  public Boolean verifyMessageDetached(byte[] message, PublicKey publicKey, byte[] signature) throws Exception {
    Signature verifier = new EdDSAEngine(MessageDigest.getInstance("SHA-512"));
    verifier.initVerify(publicKey);
    verifier.update(message);
    return verifier.verify(signature);
  }
}