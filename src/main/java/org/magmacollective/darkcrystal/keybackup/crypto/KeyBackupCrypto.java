package org.magmacollective.darkcrystal.keybackup.crypto;

import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.GeneralSecurityException;
import org.bouncycastle.crypto.engines.XSalsa20Engine;
import org.bouncycastle.crypto.macs.Poly1305;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.digests.Blake2bDigest;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.whispersystems.curve25519.*;

/**
 * Crypto methods for Dark Crystal Distributed Key Backup
 * @author Magma Collective
 */
public class KeyBackupCrypto {
  public KeyBackupCrypto() {
  }

  public static final int SYMMETRIC_KEY_BYTES = 32;
  public static final int NONCE_BYTES = 24;
  public static final int MAC_BYTES = 16;
  public static final int BLAKE2B_BYTES = 32;
  public static final int CURVE25519_PUBLIC_KEY_BYTES = 32;
  public static final int CURVE25519_SECRET_KEY_BYTES = 32;

  /**
   * Generates a Curve25519 keypair
   * @return the keypair object
   */
  public static Curve25519KeyPair generateCurve25519Keypair() {
    return Curve25519.getInstance(Curve25519.BEST).generateKeyPair();
  }

  /**
   * Calculate a DH agreement
   * @param publicKey
   * @param privateKey
   * @return the shared secret
   */
  public static byte[] calculateAgreement (byte[] publicKey, byte[] privateKey) {
    Curve25519 cipher = Curve25519.getInstance(Curve25519.BEST);
    return cipher.calculateAgreement(publicKey, privateKey);
  }

  /**
   * 32 byte Blake2b hash without key
   * @param message
   * @return 32 byte hash
   */
  public static byte[] blake2b (byte[] message) {
    Blake2bDigest digest = new Blake2bDigest(null, BLAKE2B_BYTES, null, null);
    digest.update(message, 0, message.length);
    byte[] hash = new byte[BLAKE2B_BYTES];
    digest.doFinal(hash, 0);
    return hash;
  }

  /**
   * 32 byte Blake2b keyed hash
   * @param message
   * @param key
   * @return 32 byte hash
   */
  public static byte[] blake2b (byte[] message, byte[] key) {
    Blake2bDigest digest = new Blake2bDigest(key, BLAKE2B_BYTES, null, null);
    digest.update(message, 0, message.length);
    byte[] hash = new byte[BLAKE2B_BYTES];
    digest.doFinal(hash, 0);
    return hash;
  }

  /**
   * encrypt a message to the given recipient
   * @param message
   * @param keypair your own Curve25519 keypair
   * @param recipientPublicKey
   * @return the ciphertext containing integrated MAC
   */
  public static byte[] box (byte[] message, Curve25519KeyPair keypair, byte[] recipientPublicKey) {
    byte[] dhAgreement = calculateAgreement(recipientPublicKey, keypair.getPrivateKey());
    byte[] sharedSecret = blake2b(byteArrayConcat(keypair.getPublicKey(), recipientPublicKey), dhAgreement);
    return encrypt(message, sharedSecret);
  }

  /**
   * encrypt a message to the given recipient, with additional contextual data
   * @param message
   * @param keypair your own Curve25519 keypair
   * @param recipientPublicKey
   * @param contextData additional contextual data known by both parties
   * @return the ciphertext containing integrated MAC
   */
  public static byte[] box (byte[] message, Curve25519KeyPair keypair, byte[] recipientPublicKey, byte[] contextData) {
    byte[] dhAgreement = calculateAgreement(recipientPublicKey, keypair.getPrivateKey());
    byte[] sharedSecret = blake2b(
            byteArrayConcat(byteArrayConcat(keypair.getPublicKey(), recipientPublicKey),
                    contextData), dhAgreement);
    return encrypt(message, sharedSecret);
  }

  /**
   * decrypt a message from a given sender
   * @param ciphertext the ciphertext with integrated MAC
   * @param keypair your own curve25519 keypair
   * @param senderPublicKey
   * @return plaintext message
   */
  public static byte[] unbox (byte[] ciphertext, Curve25519KeyPair keypair, byte[] senderPublicKey)
        throws GeneralSecurityException {
    byte[] dhAgreement = calculateAgreement(senderPublicKey, keypair.getPrivateKey());
    byte[] sharedSecret = blake2b(byteArrayConcat(senderPublicKey, keypair.getPublicKey()), dhAgreement);
    return decrypt(ciphertext, sharedSecret);
  }

  /**
   * decrypt a message from a given sender, with additional contextual data
   * @param ciphertext the ciphertext with integrated MAC
   * @param keypair your own curve25519 keypair
   * @param senderPublicKey
   * @param contextData additional contextual data known by both parties
   * @return plaintext message
   */
  public static byte[] unbox (byte[] ciphertext, Curve25519KeyPair keypair, byte[] senderPublicKey, byte[] contextData)
        throws GeneralSecurityException {
    byte[] dhAgreement = calculateAgreement(senderPublicKey, keypair.getPrivateKey());
    byte[] sharedSecret = blake2b(
            byteArrayConcat(byteArrayConcat(senderPublicKey, keypair.getPublicKey()),
                    contextData), dhAgreement);
    return decrypt(ciphertext, sharedSecret);
  }


  /**
   * Generate a random key
   * @return a 32 byte key
   */
  public static byte[] generateSymmetricKey() {
    final byte[] k = new byte[SYMMETRIC_KEY_BYTES];
    final SecureRandom random = new SecureRandom();
    random.nextBytes(k);
    return k;
  }

  /**
   * Generate a random nonce
   * @return a 24 byte nonce
   */
  public static byte[] generateNonce() {
    final byte[] n = new byte[NONCE_BYTES];
    final SecureRandom random = new SecureRandom();
    random.nextBytes(n);
    return n;
  }

  /**
   * encrypt a message with the given symmetric key
   * @param message
   * @param key
   * @return the ciphertext with integrated MAC and nonce
   */
  public static byte[] encrypt(byte[] message, byte[] key) {
    final byte[] nonce = generateNonce();
    return byteArrayConcat(nonce, secretBox(message, key, nonce));
  }

  /**
   * Attempt to decrypt a message with a given symmetric key
   * @param ciphertextWithNonce ciphertext with integrated MAC and nonce
   * @param key
   * @return either the plaintext or
   */
  public static byte[] decrypt(byte[] ciphertextWithNonce, byte[] key) throws GeneralSecurityException {
    final byte[] nonce = new byte[NONCE_BYTES];
    System.arraycopy(ciphertextWithNonce, 0, nonce, 0, NONCE_BYTES);
    final byte[] ciphertext = new byte[ciphertextWithNonce.length - NONCE_BYTES];
    System.arraycopy(ciphertextWithNonce, NONCE_BYTES, ciphertext, 0, ciphertext.length);
    return secretUnbox(key, nonce, ciphertext);
  }

  /**
   * Encrypt a message with a given symmetric key and nonce
   * @param message
   * @param key
   * @param nonce
   * @return ciphertext with integrated MAC
   */
  public static byte[] secretBox(byte[] message, byte[] key, byte[] nonce) {
    final XSalsa20Engine xsalsa20 = new XSalsa20Engine();
    xsalsa20.init(true, new ParametersWithIV(new KeyParameter(key), nonce));

    // generate Poly1305 subkey
    final byte[] sk = new byte[SYMMETRIC_KEY_BYTES];
    xsalsa20.processBytes(sk, 0, SYMMETRIC_KEY_BYTES, sk, 0);

    final byte[] out = new byte[message.length + MAC_BYTES];
    xsalsa20.processBytes(message, 0, message.length, out, MAC_BYTES);

    // hash ciphertext and prepend mac to ciphertext
    final Poly1305 poly1305 = new Poly1305();
    poly1305.init(new KeyParameter(sk));
    poly1305.update(out, MAC_BYTES, message.length);
    poly1305.doFinal(out, 0);
    return out;
  }

  /**
   * Decrypt a message with the given symmetric key and nonce
   * @param key
   * @param nonce
   * @param ciphertext
   * @return either plaintext or
   */
  public static byte[] secretUnbox(byte[] key, byte[] nonce, byte[] ciphertext)
          throws GeneralSecurityException {
    final XSalsa20Engine xsalsa20 = new XSalsa20Engine();
    final Poly1305 poly1305 = new Poly1305();

    // initialize XSalsa20
    xsalsa20.init(false, new ParametersWithIV(new KeyParameter(key), nonce));

    // generate mac subkey
    final byte[] sk = new byte[SYMMETRIC_KEY_BYTES];
    xsalsa20.processBytes(sk, 0, sk.length, sk, 0);

    // hash ciphertext
    poly1305.init(new KeyParameter(sk));
    final int len = Math.max(ciphertext.length - MAC_BYTES, 0);
    poly1305.update(ciphertext, MAC_BYTES, len);
    final byte[] calculatedMAC = new byte[MAC_BYTES];
    poly1305.doFinal(calculatedMAC, 0);

    // extract mac
    final byte[] presentedMAC = new byte[MAC_BYTES];
    System.arraycopy(ciphertext, 0, presentedMAC, 0, Math.min(ciphertext.length, MAC_BYTES));

    // compare macs
    if (!MessageDigest.isEqual(calculatedMAC, presentedMAC)) {
      throw new GeneralSecurityException("Decryption failed");
    }

    // decrypt ciphertext
    final byte[] plaintext = new byte[len];
    xsalsa20.processBytes(ciphertext, MAC_BYTES, plaintext.length, plaintext, 0);
    return plaintext;
  }

  /**
   * Concatonate two byte arrays
   * @param array1
   * @param array2
   * @return array1 concatonated with array2
   */
  public static byte[] byteArrayConcat(byte[] array1, byte[] array2) {
    byte[] result = new byte[array1.length + array2.length];
    System.arraycopy(array1, 0, result, 0, array1.length);
    System.arraycopy(array2, 0, result, array1.length, array2.length);
    return result;
  }

  /**
   * Encrypt a message to the given recipient using an ephemeral keypair,
   * and attach the ephemeral public key to the message, so that it can be
   * decrypted by the recipient without revealing who it is from.
   * @param message
   * @param recipientPublicKey
   * @return ciphertext including ephemeral public key, nonce and MAC
   */
  public static byte[] oneWayBox(byte[] message, byte[] recipientPublicKey) {
    final Curve25519KeyPair ephemeralKeyPair = generateCurve25519Keypair();
    // TODO remove secret key from memory
    return byteArrayConcat(
            ephemeralKeyPair.getPublicKey(),
            box(message, ephemeralKeyPair, recipientPublicKey)
            );
  }

  /**
   * Decrypt a message encrypted with 'oneWayBox'
   * @param ciphertextWithKey
   * @param keyPair of recipient
   * @return plaintext
   */
  public static byte[] oneWayUnbox(byte[] ciphertextWithKey, Curve25519KeyPair keyPair)
          throws GeneralSecurityException {
    final byte[] ephemeralPublicKey = new byte[CURVE25519_PUBLIC_KEY_BYTES];
    final byte[] ciphertext = new byte[ciphertextWithKey.length - CURVE25519_PUBLIC_KEY_BYTES];
    System.arraycopy(ciphertextWithKey, 0, ephemeralPublicKey, 0, CURVE25519_PUBLIC_KEY_BYTES);
    System.arraycopy(ciphertextWithKey, CURVE25519_PUBLIC_KEY_BYTES, ciphertext, 0, ciphertext.length);
    return unbox(ciphertext, keyPair, ephemeralPublicKey);
  }

  /**
   * Convenience method to encode a byte array to a hexadecimal string
   * @param input a byte array
   * @return a hexadecimal encoded string
   */
  public static String toHexString(byte[] input) {
    return Hex.toHexString(input);
  }

  /**
   * Convenience method to convert a hexadecimal encoded string to a byte array
   * @param input a hexadecimal encoded string
   * @return a byte array
   */
  public static byte[] fromHexString(String input) {
    return Hex.decode(input);
  }

  /**
   * Convenience method to encode a byte array to a Base64 encoded string
   * @param input a byte array
   * @return a Base64 encoded string
   */
  public static String toBase64String(byte[] input) {
    return Base64.toBase64String(input);
  }

  /**
   * Convencience method to convert a Base64 encoded string to a byte array
   * @param input a Base64 encoded string
   * @return a byte array
   */
  public static byte[] fromBase64String(String input) {
    return Base64.decode(input);
  }
}