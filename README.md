
# Cryptographic functions for Dark Crystal Key Backup 

- [API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/index.html)
- Further explanation can be found in the [Dark Crystal Key Backup Protocol Specification](https://gitlab.com/dark-crystal-java/key-backup-protocol-specification)

## Includes: 

### Authenticated symmetric encryption

Similar to NaCl's `secretbox`

### Diffie Hellman

- Generating keys Curve25519 Keys and calculating a DH shared secret with `org.whispersystems.curve25519`
- A higher level crypto agreement based on this primitive

### EdDSA Signing and verification

- With 'attached' or 'detached' signatures (stored together with the message, or apart).

## Hashing

- `blake2b`, with or without given key.

## 'one-way-box'

A higher level operation used to encrypt a message allowing a given recipient to decrypt it without knowing the identity of the sender. This is useful because it reduces the metadata required to be carried with the message, obfuscating the identity of the sender from an eavesdropper.

After the sender has created the message, they are not able to decrypt it themselves (assuming they discard the ephemeral private key). This is discussed in more detail in the [protocol specification](https://gitlab.com/dark-crystal-java/key-backup-protocol-specification).

## String encoding

- as hex or base64

## Publishing to Maven

To upload the to Maven, prepare as follows.

Add the following content to your `local.properties` file:

    local.maven.publishing.dir=file:///path/to/maven/repo

In file `build.gradle`, adapt values in the `ext` stanza. The coordinates of
the Maven artifact are defined in file `gradle.properties`.
Increment the `pVersion` variable when uploading a new version.

Then run:

    ./gradlew clean publish

Afterwards commit changes to the Maven repo git and publish to the Maven
server.
